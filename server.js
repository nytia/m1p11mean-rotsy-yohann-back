const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const PORT = process.env.PORT || 5000;
const serv= require('./routes/serviceRoutes');
const auth= require('./routes/authRoutes');
const cli= require('./routes/clientRoutes');
const rd= require('./routes/rdvRoutes');


dotenv.config();

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan("common"));

app.use('/service',serv);
app.use('/auth',auth);
app.use('/client',cli);
app.use('/rdv',rd);

// mongoose.set("strictQuery", false);
mongoose.connect(process.env.MONGO_URL)
  .then(() => {
    console.log("Mongodb is connected");
  })
  .catch((err) => {
    console.error(err);
  });

app.listen(PORT, () => {
  console.log('Server running on port '+PORT);
});
