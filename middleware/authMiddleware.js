const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) {
    return res.status(401).json({ error: 'Accès non autorisé. Token manquant.' });
  }

  jwt.verify(token, 'votre-secret-jwt', (err, decoded) => {
    if (err) {
      return res.status(401).json({ error: 'Token invalide.' });
    }
    req.client = decoded.client;
    next();
  });
};

module.exports = authMiddleware;
