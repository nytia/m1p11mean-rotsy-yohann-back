const express = require('express')
const router = express.Router()
const Rdv = require('../models/rdv')
const asyncHandler = require('express-async-handler')
//POST Upload Route
router.post('/add', (req, res) =>{
    try {
  
      const result=req.body;
      //Create Uploads
      let rd = new Rdv({
        idClient: result.idClient,
        dateRdv: result.dateRdv,
        idService: result.idService
      })
      //Save Upload
      const createdRdv =  rd.save();
      res.status(201).json(createdRdv);
    } catch (err) {
      console.log(err);
    }
  
  })

  module.exports = router