const express = require('express')
const router = express.Router()
const asyncHandler = require('express-async-handler')
const Client = require('../models/client')

router.get('/getAll', asyncHandler(async(req,res) => {
    try {
      let allclients = await Client.find();
      res.json(allclients)
    } catch (err) {
      console.log(err)
      throw new Error('Client Cannot Be Found')
    }
}))

module.exports = router