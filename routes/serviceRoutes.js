const express = require('express')
const router = express.Router()
const Service = require('../models/service')
const asyncHandler = require('express-async-handler')
// const authMiddleware = require('../middleware/authMiddleware')
// const bodyParser=require('body-parser')

//POST Upload Route
router.post('/add', (req, res) =>{
  try {

    const result=req.body;
    //Create Uploads
    let se = new Service({
      nom: result.nom,
      prix: result.prix,
      commission: result.commission,
      duree: result.duree
    })
    //Save Upload
    const createdService =  se.save();
    res.status(201).json(createdService);
  } catch (err) {
    console.log(err);
  }

})

//GET Upload Route
router.get('/getAll', asyncHandler(async(req,res) => {
    try {
      let allservices = await Service.find();
      res.json(allservices)
    } catch (err) {
      console.log(err)
      throw new Error('Service Cannot Be Found')
    }
}))


router.put('/update/:id', async (req, res) => {
  try {
    const serviceId = req.params.id;
    const updateData = req.body;

    if (!serviceId) {
      return res.status(400).json({ message: 'ID du service manquant dans la requête' });
    }

    const existingService = await Service.findById(serviceId);
    if (!existingService) {
      return res.status(404).json({ message: 'Service non trouvé' });
    }

    existingService.nom = updateData.nom || existingService.nom;
    existingService.prix = updateData.prix || existingService.prix;
    existingService.commission = updateData.commission || existingService.commission;
    existingService.duree = updateData.duree || existingService.duree;

    const updatedService = await existingService.save();

    res.status(200).json(updatedService);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Erreur serveur lors de la mise à jour du service' });
  }
});




module.exports = router