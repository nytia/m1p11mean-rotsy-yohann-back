const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Client = require('../models/client');
const Manager = require('../models/manager');


router.post('/register', async (req, res) => {
  try {
    const { nom, prenom, identifiant, mdp } = req.body;
    const hashedPassword = await  bcrypt.hash(mdp, 10);
    const newClient = new Client({ nom, prenom, identifiant, mdp: hashedPassword });
    const savedClient = await newClient.save();
    res.status(201).json(savedClient);
  } catch (error) {
    console.error("Erreur lors de l'inscription", error);
    res.status(500).json({ error: "Erreur lors de l'inscription" });
  }
});

router.post('/login', async (req, res) => {
  try {
    const { identifiant, mdp } = req.body;
    const client = await Client.findOne({ identifiant });
    if (!client) {
      return res.status(401).json({ error: 'Identifiant ou mot de passe incorrect' });
    }
    const isPasswordValid = await bcrypt.compare(mdp, client.mdp);
    if (!isPasswordValid) {
      return res.status(401).json({ error: 'Identifiant ou mot de passe incorrect' });
    }
    const token = jwt.sign({ client }, 'secret-jwt', { expiresIn: '3m' });
    res.status(200).json({ token, clientId: client._id });
  } catch (error) {
    console.error("Erreur lors de l'authentification", error);
    res.status(500).json({ error: "Erreur lors de l'authentification" });
  }
});

router.post('/loginManager', async (req, res) => {
  try {
    const { identifiant, mdp } = req.body;
    const man = await Manager.findOne({ identifiant });
    if (!man) {
      return res.status(401).json({ error: 'Identifiant ou mot de passe incorrect 1' });
    }
    const isPasswordValid = await bcrypt.compare(mdp, man.mdp);
    if (!isPasswordValid) {
      return res.status(401).json({ error: 'Identifiant ou mot de passe incorrect 2' });
    }
    const tokenman = jwt.sign({ man }, 'secret-jwt', { expiresIn: '3m' });
    res.status(200).json({ tokenman });
  } catch (error) {
    console.error("Erreur lors de l'authentification", error);
    res.status(500).json({ error: "Erreur lors de l'authentification" });
  }
});

module.exports = router;
