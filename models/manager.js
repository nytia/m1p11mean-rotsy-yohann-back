const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const managerSchema = new Schema(
    {
        nom: {
            type: String,
            required: true,
        },
        prenom: {
            type: String,
            required: true,
        },
        identifiant: {
            type: String,
            required: true,
        },
        mdp: {
            type: String,
            required: false,
        }
    }
);

module.exports = mongoose.model("Manager", managerSchema);