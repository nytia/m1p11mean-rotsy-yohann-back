const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const rdvSchema = new Schema({
    idClient: {
        type: Schema.Types.ObjectId,
        ref: 'Client',
        required: true,
    },
    dateRdv: {
        type: Date,
        required: true,
    },
    idService: {
        type: Schema.Types.ObjectId,
        ref: 'Service',
        required: true,
    },
});

module.exports = mongoose.model('Rdv', rdvSchema);
